A complete "high level" to asm (almost RISC-V) toolchain.

Background

In a project course at my university we're supposed to design and implement a
processor and its instruction set on an FPGA. Additionally, we implement some
sort of program (usually a game) targeting our processor using our own
instruction set. Most groups figure out that writing machine code and relative
jumps (absolute jumps too for that matter) quickly gets old and turn to a home
grown rudimentary assembler. I wanted to try my hand at going one step further
and create a more high level language that compiles to our instruction set.

Language features

Current:
- S-expressions.
- Weird calling conventions.

Soon:
- Recursion (yes).

Later maybe:
- Actual RISCV assembly output. (At least RV32I, maybe all the way to RV32G.)
- x86 via nasm. (Since x86 does op-assign instead of op for arithmetic it's frozen for now.)
- More custom syntax than s-expressions. Not that s-expressions are bad necessarily.
- Type system.
