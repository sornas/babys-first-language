use std::fmt;

use crate::location_info::Loc;

pub type Identifier = String;
pub type IR = Vec<Instruction>;
pub type Block = (IR, Value);

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Value {
    Named(Identifier, u32),
    Unnamed(u32),
}

impl Value {
    pub fn number(&self) -> u32 {
        match self {
            Value::Named(_, n) | Value::Unnamed(n) => *n,
        }
    }
}

impl fmt::Display for Value {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Value::Named(ident, num) => write!(f, "\"{ident}\" | {num}"),
            Value::Unnamed(num) => write!(f, "{num}"),
        }
    }
}

pub enum OuterIR {
    Function(Loc<Identifier>, IR, Vec<Loc<Value>>),
}

#[derive(Debug)]
pub enum Instruction {
    Assign(Value, Expression),
    ReadMemory {
        value: Value,
        address: Value,
    },
    WriteMemory {
        value: Value,
        address: Value,
    },
    If {
        condition: Block,
        body: IR,
        else_body: Option<IR>,
    },
    Loop {
        condition: Block,
        body: IR,
    },
    Return(Value),
}

#[derive(Debug)]
pub enum Expression {
    BinOp(BinOp, Value, Value),
    ConstantI32(i32),
    ConstantU32(u32),
    Value(Value),
    Call(Identifier, Vec<Value>),
}

#[derive(Debug)]
pub enum BinOp {
    Add,
    Less,
}

fn print_indent(indent: usize) {
    print!(
        "{}",
        std::iter::repeat("  ")
            .take(indent)
            .collect::<Vec<&str>>()
            .join("")
    );
}

impl Instruction {
    pub fn pretty_print(&self, indent: usize) {
        print_indent(indent);
        match self {
            Instruction::Loop { condition, body } => {
                println!("Loop(");
                print_indent(indent + 1);
                println!("Cond({},", condition.1);
                for cond_expr in &condition.0 {
                    cond_expr.pretty_print(indent + 2);
                }
                print_indent(indent + 1);
                println!(")");
                print_indent(indent + 1);
                println!("Body(");
                for body_expr in body {
                    body_expr.pretty_print(indent + 2);
                }
                print_indent(indent + 1);
                println!(")");
                print_indent(indent);
                println!(")");
            }
            Instruction::If {
                condition,
                body,
                else_body,
            } => {
                println!("If(");
                print_indent(indent + 1);
                println!("Cond({},", condition.1);
                for cond_expr in &condition.0 {
                    cond_expr.pretty_print(indent + 2);
                }
                print_indent(indent + 1);
                println!(")");
                print_indent(indent + 1);
                println!("Body(");
                for body_expr in body {
                    body_expr.pretty_print(indent + 2);
                }
                print_indent(indent + 1);
                println!(")");
                if let Some(else_body) = else_body {
                    print_indent(indent + 1);
                    println!("Else(");
                    for body_expr in else_body {
                        body_expr.pretty_print(indent + 2);
                    }
                    print_indent(indent + 1);
                    println!(")");
                }
                print_indent(indent);
                println!(")");
            }
            inst => {
                println!("{:?}", inst);
            }
        }
    }
}
