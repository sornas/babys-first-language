use std::collections::HashMap;

use crate::{ast, ir, location_info};
use crate::{Diagnostic, IdTracker};

use ast::{Identifier, Node};
use codespan_reporting::diagnostic::Label;
use ir::{BinOp, Expression, Instruction, OuterIR, Value, IR};
use location_info::{Loc, WithLocation};

type Variables = HashMap<Loc<Identifier>, Value>;
type Functions = HashMap<Loc<Identifier>, Vec<Loc<Value>>>;

fn extract_function(
    node: &Loc<ast::Node>,
    idgen: &mut IdTracker,
) -> Result<Option<(Loc<Identifier>, Vec<Loc<Value>>)>, Vec<Diagnostic>> {
    match node.strip_ref() {
        Node::Integer(_) => Ok(None),
        Node::HexInteger(_) => Ok(None),
        Node::Identifier(_) => Ok(None),
        Node::NodeList(nodes) => {
            if nodes.get(0).and_then(|node| node.identifier()) != Some("def") {
                return Ok(None);
            }
            if nodes.len() != 4 {
                return Err(vec![Diagnostic::error()
                    .with_message("`def` has wrong amount of arguments")
                    .with_labels(vec![
                        Label::primary((), node.loc()).with_message("this function definition")
                    ])
                    .with_notes(vec![format!(
                        "expected {} arguments, got {}",
                        4,
                        nodes.len()
                    )])]);
            }
            let ident = nodes[1].identifier().ok_or_else(|| {
                vec![Diagnostic::error()
                    .with_message("function name must be a valid identifier")
                    .with_labels(vec![
                        Label::primary((), nodes[1].loc()).with_message("not a valid identifier")
                    ])]
            })?;
            let parameters: Vec<Loc<Value>> = nodes[2]
                .node_list()
                .unwrap()
                .iter()
                .map(|node| node.identifier().unwrap().to_string().at(node))
                .zip(std::iter::repeat_with(|| idgen.next()))
                .map(|(ident, value)| ident.map(|ident| Value::Named(ident, value)))
                .collect();
            Ok(Some((ident.to_string().at(node), parameters)))
        }
    }
}

fn outer_statement(node: &Loc<ast::Node>, idgen: &mut IdTracker, functions: &Functions) -> OuterIR {
    match node.strip_ref() {
        Node::Integer(_) => panic!(),
        Node::HexInteger(_) => panic!(),
        Node::Identifier(_) => panic!(),
        Node::NodeList(nodes) => {
            let func = nodes[0].identifier().unwrap();
            assert_eq!(func, "def");
            let ident = nodes[1].identifier().unwrap().to_string().at(node);
            let parameters = functions.get(&ident).unwrap();
            let ir = function_body(&nodes[3], parameters, idgen, functions);
            OuterIR::Function(ident, ir, parameters.clone())
        }
    }
}

fn function_body(
    node: &Loc<ast::Node>,
    parameters: &[Loc<Value>],
    idgen: &mut IdTracker,
    functions: &Functions,
) -> IR {
    let mut variables = parameters
        .iter()
        .cloned()
        .map(|value| match value.strip_ref() {
            Value::Named(ident, _) => (ident.to_string().at(&value), value.strip()),
            Value::Unnamed(_) => panic!("parameter is an unnamed value"),
        })
        .collect();
    let mut ir = inner_statement(node, idgen, &mut variables, functions);
    if !matches!(ir.last(), Some(Instruction::Return(_))) {
        let id = Value::Unnamed(idgen.next());
        ir.push(Instruction::Assign(id.clone(), Expression::ConstantI32(0)));
        ir.push(Instruction::Return(id));
    }
    ir
}

fn inner_statement(
    node: &Loc<ast::Node>,
    idgen: &mut IdTracker,
    variables: &mut Variables,
    functions: &Functions,
) -> IR {
    expression(node, idgen, variables, functions).0
}

fn expression(
    node: &Loc<ast::Node>,
    idgen: &mut IdTracker,
    variables: &mut Variables,
    functions: &Functions,
) -> (IR, Value) {
    match node.strip_ref() {
        Node::Integer(i) => {
            let id = idgen.next();
            (
                vec![Instruction::Assign(
                    Value::Unnamed(id),
                    Expression::ConstantI32(*i),
                )],
                Value::Unnamed(id),
            )
        }
        Node::HexInteger(h) => {
            let id = idgen.next();
            (
                vec![Instruction::Assign(
                    Value::Unnamed(id),
                    Expression::ConstantU32(*h),
                )],
                Value::Unnamed(id),
            )
        }
        Node::Identifier(ident) => {
            // check if the variable is declared
            (
                vec![],
                variables.get(&ident.to_string().nowhere()).unwrap().clone(),
            )
        }
        Node::NodeList(nodes) => function_call(nodes, idgen, variables, functions),
    }
}

fn function_call(
    nodes: &[Loc<ast::Node>],
    idgen: &mut IdTracker,
    variables: &mut Variables,
    functions: &Functions,
) -> (IR, Value) {
    let ident = nodes[0].identifier().unwrap();

    if ident == "let" {
        let var = nodes[1].identifier().unwrap();
        let id = idgen.next();
        let value = Value::Named(var.to_string(), id);
        // ordering here allows stuff like (let a (add a 1))
        // maybe set isn't useful at all?
        //
        // let/set doesn't make sense without scoping
        let (mut code, assign_value) = expression(&nodes[2], idgen, variables, functions);
        let _ = variables.insert(var.to_string().at(&nodes[1]), value.clone());
        code.push(Instruction::Assign(
            value.clone(),
            Expression::Value(assign_value),
        ));
        return (code, value);
    }

    if ident == "set" {
        let var = nodes[1].identifier().unwrap();
        let (mut code, value) = expression(&nodes[2], idgen, variables, functions);
        let id = variables.get(&var.to_string().nowhere()).unwrap();
        code.push(Instruction::Assign(id.clone(), Expression::Value(value)));
        return (code, id.clone());
    }

    if ident == "if" {
        let id = idgen.next();
        let condition = expression(&nodes[1], idgen, variables, functions);
        //TODO body is expression and assign to if's id
        let body = inner_statement(&nodes[2], idgen, variables, functions);
        let else_body = nodes
            .get(3)
            .map(|node| inner_statement(node, idgen, variables, functions));
        let code = vec![Instruction::If {
            condition,
            body,
            else_body,
        }];
        return (code, Value::Unnamed(id));
    }

    if ident == "while" {
        //TODO move to inner_statement?
        let id = idgen.next();
        let condition = expression(&nodes[1], idgen, variables, functions);
        let body = inner_statement(&nodes[2], idgen, variables, functions);
        let code = vec![Instruction::Loop { condition, body }];
        return (code, Value::Unnamed(id));
    }

    if ident == "do" {
        let mut code = Vec::new();
        let mut last_value = None;
        for node in &nodes[1..] {
            let (mut expr_code, expr_value) = expression(node, idgen, variables, functions);
            code.append(&mut expr_code);
            let _ = last_value.insert(expr_value);
        }
        return (code, last_value.expect("empty `do` not allowed"));
    }

    // "normal" function calls with expression parameters

    let mut code = Vec::new();
    let mut values = Vec::new();
    for node in &nodes[1..] {
        let (mut param_code, param_value) = expression(node, idgen, variables, functions);
        code.append(&mut param_code);
        values.push(param_value);
    }

    let id = Value::Unnamed(idgen.next());
    let return_id = id.clone();
    //TODO check number of parameters
    match ident {
        "add" => {
            code.push(Instruction::Assign(
                id,
                Expression::BinOp(BinOp::Add, values[0].clone(), values[1].clone()),
            ));
        }
        "less" => {
            code.push(Instruction::Assign(
                id,
                Expression::BinOp(BinOp::Less, values[0].clone(), values[1].clone()),
            ));
        }
        "read" => code.push(Instruction::ReadMemory {
            value: id,
            address: values[0].clone(),
        }),
        "write" => code.push(Instruction::WriteMemory {
            value: values[0].clone(),
            address: values[1].clone(),
        }),
        func => {
            let wanted_params = functions
                .get(&func.to_string().nowhere())
                .expect(&format!("unknown function {func}"))
                .len();
            assert_eq!(wanted_params, values.len());
            code.push(Instruction::Assign(
                id,
                Expression::Call(func.to_string(), values),
            ))
        }
    }
    (code, return_id)
}

pub fn lower(nodes: Vec<&Loc<ast::Node>>) -> Result<Vec<OuterIR>, Vec<Diagnostic>> {
    let mut idgen = IdTracker(0);
    let mut diagnostics = Vec::new();

    let functions = {
        let mut functions = HashMap::new();
        for node in &nodes {
            match extract_function(node, &mut idgen) {
                Ok(Some((ident, params))) => {
                    functions.insert(ident, params);
                }
                Ok(None) => (),
                Err(mut d) => diagnostics.append(&mut d),
            }
        }
        functions
    };
    if !diagnostics.is_empty() {
        return Err(diagnostics);
    }

    let mut ir = Vec::new();
    for node in nodes {
        ir.push(outer_statement(node, &mut idgen, &functions));
    }
    Ok(ir)
}
