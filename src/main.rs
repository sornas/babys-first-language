use codespan_reporting::files::SimpleFile;
use codespan_reporting::term;
use codespan_reporting::term::termcolor::ColorChoice;
use codespan_reporting::term::termcolor::StandardStream;
use lalrpop_util::lalrpop_mod;

lalrpop_mod!(pub lang);

mod asm;
mod ast;
mod ast_lowering;
mod ir;
mod ir_lowering;
mod location_info;
mod register_allocation;

type Diagnostic = codespan_reporting::diagnostic::Diagnostic<()>;

#[derive(Clone)]
pub struct IdTracker(u32);

impl IdTracker {
    pub fn next(&mut self) -> u32 {
        let prev = self.0;
        self.0 = self.0 + 1;
        prev
    }
}

fn main() {
    // println!("{:?}", lang::node_listParser::new().parse("()"));
    // println!("{:?}", lang::node_listParser::new().parse("(add 1 1)"));

    // test("(add 1 2)");
    // test("(let a (add 1 2)) (let b (add a a))");
    // test("(do (let a (add 1 2)) (let b (add a a)))");
    // test("(def f () (add 1 2))
    //       (def g () (do (let a 1) (let b 2) (add a b)))
    //       (def h () (add (add 1 2) (add (f) (g))))
    // ");
    let source = "
        (def 1 () int (do
            2
        ))
        (def 2 () (do
            2
        ))
    ";
    let file = codespan_reporting::files::SimpleFile::new("main", source);
    match test(&file) {
        Ok(()) => (),
        Err(diagnostics) => {
            let writer = StandardStream::stderr(ColorChoice::Always);
            let config = codespan_reporting::term::Config::default();

            for diagnostic in &diagnostics {
                term::emit(&mut writer.lock(), &config, &file, diagnostic).unwrap();
            }
        }
    }
}

fn test<A, B>(file: &SimpleFile<A, B>) -> Result<(), Vec<Diagnostic>>
where
    A: std::fmt::Display,
    B: AsRef<str>,
{
    let ast = lang::node_listParser::new()
        .parse(file.source().as_ref())
        .unwrap();
    let ir = ast_lowering::lower(ast.iter().collect())?;
    // for outer in &ir {
    //     match outer {
    //         ir::OuterIR::Function(label, ir, _parameters) => {
    //             println!("{label}:");
    //             for node in ir {
    //                 node.pretty_print(0);
    //             }
    //         }
    //     }
    //     println!("");
    // }
    ir_lowering::fake_asm(&ir);
    Ok(())
}
