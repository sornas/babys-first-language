use std::fmt;

use bounded_integer::BoundedU8;

pub type Constant = i16;
pub type Address = u16;

pub type NamedRegister = BoundedU8<0, 29>;

#[derive(Clone, Copy, Debug)]
pub enum Register {
    Named(NamedRegister),
    StackPointer,
    Return,
}

#[derive(Clone, Debug)]
pub enum Instruction {
    /// `rd <= ra + rb`
    Add {
        rd: Register,
        ra: Register,
        rb: Register,
    },

    CompareImmediate {
        ra: Register,
        val: Constant,
    },

    /// `rd <= ra`
    Mov {
        rd: Register,
        ra: Register,
    },

    /// `rd <= #val`
    MovLo {
        rd: Register,
        val: Constant,
    },
    /// `rd <= #val << 16`
    MovHi {
        rd: Register,
        val: Constant,
    },

    /// `rd <= mem(adr)`
    LoadWord {
        rd: Register,
        adr: Address,
    },
    /// `mem(adr) <= ra`
    StoreWord {
        ra: Register,
        adr: Address,
    },

    BreakEqual(String),
    BreakNotEqual(String),

    Call(String),
    Jump(String),
    Label(String),
    Ret,

    Push(Vec<Register>),
    Pop(Vec<Register>),
}

impl fmt::Display for Register {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Register::Named(named) => write!(f, "r{named}"),
            Register::StackPointer => todo!(),
            Register::Return => write!(f, "r31"),
        }
    }
}

impl fmt::Display for Instruction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use Instruction::*;
        match self {
            Add { rd, ra, rb } => write!(f, "add {rd} {ra} {rb}"),
            CompareImmediate { ra, val } => write!(f, "cmpi {ra} {val}"),

            Mov { rd, ra } => write!(f, "mov {rd} {ra}"),

            MovLo { rd, val } => write!(f, "movlo {rd} {val}"),
            MovHi { rd, val } => write!(f, "movhi {rd} {val}"),

            LoadWord { rd, adr } => write!(f, "lw {rd} {adr}"),
            StoreWord { ra, adr } => write!(f, "sw {adr} {ra}"),

            BreakEqual(label) => write!(f, "beq {label}"),
            BreakNotEqual(label) => write!(f, "bne {label}"),

            Call(label) => write!(f, "call {label}"),
            Jump(label) => write!(f, "jmp {label}"),
            Label(label) => write!(f, "{label}:"),
            Ret => write!(f, "ret"),

            Push(regs) => write!(
                f,
                "push {{ {} }}",
                regs.iter()
                    .map(|r| format!("{r}"))
                    .collect::<Vec<String>>()
                    .join(", ")
            ),
            Pop(regs) => write!(
                f,
                "pop {{ {} }}",
                regs.iter()
                    .map(|r| format!("{r}"))
                    .collect::<Vec<String>>()
                    .join(", ")
            ),
        }
    }
}
