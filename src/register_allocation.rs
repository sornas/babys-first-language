use std::collections::{HashMap, HashSet, VecDeque};

use crate::{ir, location_info::Loc};

use ir::{Expression, Instruction, IR};

type Color = u32;
type Value = u32;

#[derive(Debug)]
struct ValueLiveRange {
    first_write: u32,
    last_read: Option<u32>,
}

fn live_range(ir: &IR, parameters: &[&ir::Value]) -> HashMap<Value, ValueLiveRange> {
    let mark_write = |ranges: &mut HashMap<Value, ValueLiveRange>, value: &ir::Value, i: u32| {
        let _ = ranges.entry(value.number()).or_insert(ValueLiveRange {
            first_write: i,
            last_read: None,
        });
    };
    let mark_read = |ranges: &mut HashMap<Value, ValueLiveRange>, value: &ir::Value, i: u32| {
        let _ = ranges.get_mut(&value.number()).unwrap().last_read.insert(i);
    };

    let mut ranges = HashMap::new();

    for param in parameters {
        mark_write(&mut ranges, param, 0);
    }

    for (i, inst) in ir.iter().enumerate() {
        let ranges = &mut ranges;
        let i = i as u32 + 1;
        match inst {
            Instruction::Assign(value, expression) => {
                mark_write(ranges, value, i);
                match expression {
                    Expression::BinOp(_, value1, value2) => {
                        mark_read(ranges, value1, i);
                        mark_read(ranges, value2, i);
                    }
                    Expression::Value(value) => {
                        mark_read(ranges, value, i);
                    }
                    Expression::Call(_, params) => {
                        for param in params {
                            mark_read(ranges, param, i);
                        }
                    }
                    Expression::ConstantI32(_) => (),
                    Expression::ConstantU32(_) => (),
                }
            }
            Instruction::ReadMemory { value, address } => {
                mark_write(ranges, value, i);
                mark_read(ranges, address, i);
            }
            Instruction::WriteMemory { value, address } => {
                mark_read(ranges, value, i);
                mark_read(ranges, address, i);
            }
            Instruction::Return(value) => {
                mark_read(ranges, value, i);
            }
            _ => todo!("only linear code is supported"),
        }
    }
    ranges
}

fn live_together(
    ranges: &HashMap<Value, ValueLiveRange>,
    amount_irs: usize,
) -> HashMap<Value, HashSet<Value>> {
    let mut active_at_point: Vec<HashSet<Value>> = vec![HashSet::new(); amount_irs + 1];
    for (
        value,
        ValueLiveRange {
            first_write,
            last_read,
        },
    ) in ranges
    {
        let last_read = last_read.unwrap_or(amount_irs as u32 + 1); //TODO unused variables can be eliminated
        for i in *first_write..last_read {
            active_at_point[i as usize].insert(*value);
        }
    }

    let mut neighbours: HashMap<Value, HashSet<Value>> = HashMap::new();
    for active in &active_at_point {
        //TODO variable naming
        for value in active {
            for other in active {
                neighbours
                    .entry(*value)
                    .or_insert(HashSet::new())
                    .insert(*other);
            }
        }
    }

    neighbours
}

fn color(
    graph: &HashMap<Value, HashSet<Value>>,
    max_colors: u32,
) -> (HashMap<Value, Color>, Vec<Value>) {
    let all_colors: HashSet<Color> = (0..max_colors).collect();

    let mut left_to_color: VecDeque<(u32, Value)> = graph
        .iter()
        .map(|(val, neighbours)| (neighbours.len() as u32, *val))
        .collect();
    let mut colors: HashMap<Value, Color> = HashMap::new();
    let mut _spilled = Vec::new();

    left_to_color.make_contiguous().sort();

    // iteratively
    while !left_to_color.is_empty() {
        // color all nodes with < max_colors neighbours and remove them from the graph
        let mut colored = Vec::new();
        for (_, value) in left_to_color
            .iter()
            .cloned()
            .filter(|(neighbours, _)| neighbours < &max_colors)
        {
            // these values can always be colored
            // find a color not present in its neighbours
            let colors_among_neighbours: HashSet<Color> = graph
                .get(&value)
                .unwrap()
                .iter()
                .filter_map(|value| colors.get(value).copied())
                .collect();
            let color = *all_colors
                .difference(&colors_among_neighbours)
                .next()
                .unwrap();
            colors.insert(value, color);
            colored.push(value);
        }
        left_to_color.retain(|(_, value)| !colored.contains(value));
        if !left_to_color.is_empty() {
            todo!("i don't know how to spill values");
        }
    }

    assert!(colors.len() + _spilled.len() == graph.len());
    (colors, _spilled)
}

pub fn register_allocation(
    ir: &IR,
    parameters: &[Loc<ir::Value>],
    max_colors: u32,
) -> (HashMap<Value, Color>, Vec<Value>) {
    // for each value, find where it is used first and read last
    // go through each program point from top to bottom
    //  when a value is written: set `first_write` if not set
    //  when a value is read: update `last_read` to point to this instruction
    // (last_read == None => never used)
    // parameters are said to be written before the function runs and as such
    // live until they are used last. (in the ir they will be "entry points", much like constants.)
    // branches:
    //                A
    //               / \
    //              B   E
    //             /|   |\
    //            C D   F G
    // a value used in D lives after B and A. a value used in E lives after A.
    // recurse when reaching if/loop.
    // loops are easier since they are run top -> down. (important that condition lives after the
    // body too)
    // probably easier to see a branch as the union of both of its branches

    // at each program point, which values are live together?

    let parameters: Vec<_> = parameters.iter().map(|param| param.strip_ref()).collect();
    let ranges: HashMap<Value, ValueLiveRange> = live_range(ir, &parameters);
    let neighbours: HashMap<Value, HashSet<Value>> = live_together(&ranges, ir.len());
    color(&neighbours, max_colors)
}
