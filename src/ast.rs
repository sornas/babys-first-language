use crate::location_info::Loc;

pub type Identifier = String;

#[derive(Clone, Debug)]
pub enum Node {
    Integer(i32),
    HexInteger(u32),
    Identifier(Identifier),
    NodeList(Vec<Loc<Node>>),
}

impl Node {
    pub fn identifier(&self) -> Option<&str> {
        match self {
            Self::Identifier(s) => Some(s),
            _ => None,
        }
    }

    pub fn node_list(&self) -> Option<&[Loc<Node>]> {
        match self {
            Self::NodeList(nl) => Some(nl),
            _ => None,
        }
    }
}
