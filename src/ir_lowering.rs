use crate::ir;
use crate::register_allocation;

use ir::{BinOp, Expression, Instruction, OuterIR};

pub fn fake_asm(ir: &[OuterIR]) {
    println!("asm:");
    let functions: Vec<_> = ir
        .iter()
        .map(|OuterIR::Function(label, nodes, parameters)| (label, nodes, parameters))
        .collect();

    for (label, ir, parameters) in functions {
        println!("{label}:");
        let coloring = register_allocation::register_allocation(ir, parameters, 25);
        let mut used_registers: Vec<_> = coloring.0.iter().map(|(_, reg)| reg).collect();
        used_registers.sort();
        let color = |value: &ir::Value| coloring.0.get(&value.number()).unwrap();

        if !parameters.is_empty() {
            println!(
                "pop {{ {} }}",
                parameters
                    .iter()
                    .map(|val| format!("r{}", color(val)))
                    .collect::<Vec<String>>()
                    .join(", ")
            );
        }

        for instruction in ir.iter() {
            match instruction {
                Instruction::Assign(rd, expr) => match expr {
                    Expression::BinOp(op, ra, rb) => match op {
                        BinOp::Add => {
                            println!("add r{} r{} r{}", color(rd), color(ra), color(rb));
                        }
                        BinOp::Less => {
                            println!("cmp r{} r{}", color(ra), color(rb));
                            println!("movlo r{} 1", color(rd));
                            println!("silt"); // skip if less than
                            println!("movlo r{} 0", color(rd));
                        }
                    },
                    Expression::ConstantI32(i) => {
                        if ((i16::MIN as i32)..=(i16::MAX as i32)).contains(i) {
                            println!("movlo r{} {}", color(rd), i);
                        } else {
                            todo!()
                        }
                    }
                    Expression::ConstantU32(u) => {
                        if *u <= u16::MAX as u32 {
                            println!("movlo r{} {}", color(rd), u);
                        } else {
                            todo!()
                        }
                    }
                    Expression::Value(ra) => {
                        println!("mov r{} r{}", color(rd), color(ra));
                    }
                    Expression::Call(ident, params) => {
                        if !params.is_empty() {
                            println!(
                                "push {{ {} }}",
                                params
                                    .iter()
                                    .map(|val| format!("r{}", color(val)))
                                    .collect::<Vec<String>>()
                                    .join(", ")
                            );
                        }
                        println!("call {ident}");
                        println!("pop {{ r{} }}", color(rd));
                    }
                },
                Instruction::ReadMemory { value, address } => {
                    println!("lw r{} r{}", color(value), color(address)); //TODO ordering?
                }
                Instruction::WriteMemory { value, address } => {
                    println!("sw r{} r{}", color(value), color(address)); //TODO ordering?
                }
                Instruction::Return(value) => {
                    println!("push r{}", color(value));
                    println!("ret");
                }
                _ => todo!(),
            }
        }
    }
}
